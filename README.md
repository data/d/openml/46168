# OpenML dataset: grace

https://www.openml.org/d/46168

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset grace from R package 'mlr3proba'

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46168) of an [OpenML dataset](https://www.openml.org/d/46168). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46168/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46168/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46168/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

